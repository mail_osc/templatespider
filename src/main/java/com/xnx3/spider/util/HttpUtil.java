package com.xnx3.spider.util;

import java.io.IOException;
import java.util.Map;

import com.xnx3.BaseVO;
import com.xnx3.spider.vo.ResponseVO;

import cn.zvo.http.Http;
import cn.zvo.http.Response;

/**
 * 网络请求
 * @author 管雷鸣
 *
 */
public class HttpUtil {
	
	/**
	 * 通过url获取其内容
	 * @param url 网页url，全部，如 http://xxxxx.com/aaa.html
	 * @param headersMap 
	 * @param useChrome 使用使用chrome内核，true使用
	 * @return result=1 则info返回内容
	 */
	public static ResponseVO getContentByUrl(String url,Map<String, String> headersMap, boolean useChrome) throws IOException{
		ResponseVO vo = new ResponseVO();
		if(useChrome) {
			ChromeUtil chrome = new ChromeUtil();
			vo = chrome.executeneibu(url);
			return vo;
		}else {
			Http http = new Http();
			Response res = http.get(url, null, headersMap);
			vo.setMimeType(res.getContentType());
			if(res.getCode() != 200) {
				com.xnx3.spider.Global.log("获取页面源代码失败，http code:"+res.getCode()+", content:" +res.getContent());
				vo.setBaseVO(BaseVO.FAILURE, "获取页面源代码失败，http code:"+res.getCode()+", content:" +res.getContent());
				return vo;
			}else {
				vo.setInfo(res.getContent());
				return vo;
			}
		}
		
		
	}

	
}
